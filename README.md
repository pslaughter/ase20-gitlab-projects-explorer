# ASE 2020 GitLab Projects Explorer

Forked from https://codesandbox.io/s/ase-gitlab-projects-testing-starter-94iwb

## Prerequisites

- [NodeJS](https://nodejs.org/en/) must be installed.
- [Yarn](https://yarnpkg.com/getting-started/install) must be installed.

## How to run

First install local dependencies

```
yarn install
```

Then you can create a development server with:

```
yarn server
```

Or run tests with:

```
yarn test:unit
```
