import { shallowMount } from "@vue/test-utils";
import { fetchProjects } from "@/service";
import App from "@/App.vue";
import Loader from "@/components/Loader.vue";

jest.mock("@/service");

describe("App.vue", () => {
  let wrapper;

  const createComponent = () => {
    wrapper = shallowMount(App);
  };
  const findLoader = () => wrapper.findComponent(Loader);

  beforeEach(() => {
    fetchProjects.mockReturnValue(new Promise(() => {}));
  });

  afterEach(() => {
    wrapper.destroy();
    wrapper = null;
  });

  it("shows loader", () => {
    createComponent();

    expect(findLoader().exists()).toBe(true);
  });
});
