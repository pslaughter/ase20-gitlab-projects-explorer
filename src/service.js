const DEFAULT_PARAMS = {
  order_by: "last_activity_at"
};

/**
 * https://docs.gitlab.com/ee/api/projects.html#list-all-projects
 * @param {*} paramsArg
 */
export const fetchProjects = (paramsArg = {}) => {
  const params = Object.assign({}, DEFAULT_PARAMS, paramsArg);

  const url = new URL("https://gitlab.com/api/v4/projects");
  url.search = new URLSearchParams(params);

  // https://developer.mozilla.org/en-US/docs/Web/API/Fetch_API
  // return fetch(url).then(x => x.json())
  return fetch(url).then((res) => {
    const pageInfo = getPageInfo(res);

    return res.json().then((data) => {
      if (data.error) {
        throw new Error(data.error);
      }

      return {
        data,
        pageInfo
      };
    });
  });
};

const getPageInfo = (res) => {
  return {
    nextPage: res.headers.get("x-next-page"),
    perPage: res.headers.get("x-per-page"),
    page: res.headers.get("x-page")
  };
};
